class CajaDeSeguridad:
    '''Clase que incluye un atributo "escondido."'''
    __contraclave = "123qwe"
    
    def seguro(self, clave):
        if self.__contraclave == clave:
            print("Acceso concedido.")
        else:
            print("Acceso denegado.")

caja = CajaDeSeguridad()
print(caja.seguro("Hola"))
print(caja.seguro("123qwe"))
